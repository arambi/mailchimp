<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.5.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Mailchimp\Datasource;

use Cake\Core\InstanceConfigTrait;
use Cake\Datasource\PaginatorInterface;
use Cake\Datasource\Paginator as PaginatorOriginal;
use Cake\Datasource\Exception\PageOutOfBoundsException;

/**
 * This class is used to handle automatic model data pagination.
 */
class Paginator extends PaginatorOriginal implements PaginatorInterface
{
    
    public function pagination( $alias, $count, $numResults, $params = [], array $settings = [])
    {
        $defaults = $this->getDefaults($alias, $settings);
        $options = $this->mergeOptions($params, $defaults);
        $options = $this->checkLimit($options);

        $options += ['page' => 1, 'scope' => null];
        $options['page'] = (int)$options['page'] < 1 ? 1 : (int)$options['page'];
        list($finder, $options) = $this->_extractFinder($options);


        $page = $options['page'];
        $limit = $options['limit'];
        $pageCount = max((int)ceil($count / $limit), 1);
        $requestedPage = $page;
        $page = min($page, $pageCount);

        $sortDefault = $directionDefault = false;
        if (!empty($defaults['order']) && count($defaults['order']) === 1) {
            $sortDefault = key($defaults['order']);
            $directionDefault = current($defaults['order']);
        }

        $start = 0;
        if ($count >= 1) {
            $start = (($page - 1) * $limit) + 1;
        }
        $end = $start + $limit - 1;
        if ($count < $end) {
            $end = $count;
        }

        $paging = [
            'finder' => $finder,
            'page' => $page,
            'current' => $numResults,
            'count' => $count,
            'perPage' => $limit,
            'start' => $start,
            'end' => $end,
            'prevPage' => $page > 1,
            'nextPage' => $count > ($page * $limit),
            'pageCount' => $pageCount,
            'limit' => $defaults['limit'] != $limit ? $limit : null,
            'sortDefault' => $sortDefault,
            'directionDefault' => $directionDefault,
            'scope' => $options['scope'],
        ];

        $this->_pagingParams = [$alias => $paging];

        if ($requestedPage > $page) {
            throw new PageOutOfBoundsException([
                'requestedPage' => $requestedPage,
                'pagingParams' => $this->_pagingParams
            ]);
        }

        return $paging;
    }

}
