<div class="form-group" ng-if="data.segments">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div>
    <div ng-repeat="condition in data.content.conditions" class="mc-conditions">
      <div class="mc-conditions__field">
        <select class="form-control" ng-change="condition.type = data.merge_fields[condition.field].type" ng-model="condition.field">
          <option value="" ng-selected="!condition.field">-- Selecciona una opción --</option>
          <option ng-selected="field.tag == condition.field" ng-repeat="field in data.merge_fields" value="{{ field.tag }}">{{ field.name }}</option>
        </select>
      </div>
      <div class="mc-conditions__op" ng-if="condition.field">
        <select class="form-control" ng-model="condition.op">
          <option value="">-- Selecciona una opción --</option>
          <option ng-selected="key == condition.op" ng-repeat="(key, text) in data.mcFieldOptions[data.merge_fields[condition.field].type]" value="{{ key }}">{{ text }}</option>
        </select>
      </div>
      <div class="mc-conditions__value"  ng-if="condition.field">
        <div ng-if="data.merge_fields[condition.field].type != 'dropdown' && data.merge_fields[condition.field].type != 'radio'
          && condition.op != 'blank'
          && condition.op != 'blank_not'
        ">
          <input class="form-control" type="text" ng-model="condition.value">
        </div>

        <div ng-if="
          condition.field == key
          && condition.op != 'blank'
          && condition.op != 'blank_not'
        " ng-repeat="(key, options) in data.merge_fields_dropdown_options">
          <select class="form-control" ng-model="condition.value">
            <option value="">-- Selecciona una opción --</option>
            <option ng-selected="value == condition.value" ng-repeat="value in data.merge_fields_dropdown_options[condition.field]" value="{{ value }}">{{ value }}</option>
          </select>
        </div>
      </div>
      <div class="mc-conditions__delete">
        <span class="btn btn-danger btn-bitbucket btn-xs" confirm-delete  cf-destroy="scope.$parent.data.content.conditions.splice( scope.$index, 1)"><i class="fa fa-trash"></i></span>
      </div>
    </div>

    <span class="btn btn-default" mc-add-condition><i class="fa fa-plus"></i> Añadir</span>

  </div>
</div>
