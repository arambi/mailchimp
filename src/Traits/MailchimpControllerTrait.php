<?php 

namespace Mailchimp\Traits;

use Website\Lib\Website;
use Manager\Crud\Flash;

trait MailchimpControllerTrait
{
  
  protected function _afterSave( $content)
  {
    if( empty( $content->campaign_id))
    {
      $campaign = $this->createCampaign( $content);
      $campaign_id = $campaign ['id'];
    }
    else
    {
      $campaign_id = $content->campaign_id;
      $this->mc()->patch( '/campaigns/'. $campaign_id, [
        'settings' => [
          'subject_line' => $content->title,
          'title' => $content->title,
          'from_name' => $this->campaignFromName(),
          'reply_to' => Website::get( 'settings.mailchimp_reply_email') ? Website::get( 'settings.mailchimp_reply_email') : Website::get( 'email'),
        ],
        'recipients' => [
          'list_id' => $content->list_id,
        ],
      ]);

      $this->_updateCampaign( $content);
    }

    if( !empty( $content->schedule_time))
    {
      $time = date( 'Y-m-d H:i:s', ( is_object( $content->schedule_time) ? $content->schedule_time->toUnixString() : strtotime( $content->schedule_time)));
      $this->mc()->post( '/campaigns/'. $campaign_id . '/actions/unschedule');
      $result = $this->mc()->post( '/campaigns/'. $campaign_id . '/actions/schedule', [
        'schedule_time' => $time
      ]);
    
      if( $result ['status'] == 400)
      {
        Flash::error( 'Ha habido un error en Mailchimp y el envío no ha podido ser programado (' . $result ['detail'] . ')');
      }
    }
  }


  private function campaignFromName()
  {
    return Website::get( 'title');
  }

  private function createCampaign( $content)
  {
    $title = $content->title;
    $data = [
      'recipients' => [
        'list_id' => $content->list_id,
      ],
      'settings' => [
        'subject_line' => $title,
        'title' => $title,
        'from_name' => $this->campaignFromName(),
        'reply_to' => Website::get( 'settings.mailchimp_reply_email') ? Website::get( 'settings.mailchimp_reply_email') : Website::get( 'email'),
        // 'template_id' => (int)$content->template_id
      ],
      'type' => 'regular',
    ];

    
    // Crea campaña
    $campaign = $this->mc()->post( '/campaigns', $data);
    $this->Table->query()->update()
      ->set(['campaign_id' => $campaign ['id']])
      ->where(['id' => $content->id])
      ->execute();

    return $campaign;
  }

  protected function __beforeUpdateFind( $query, $content)
  {
    if( empty( $content->campaign_id))
    {
      $campaign = $this->createCampaign( $content);
    }
    else
    {
      $campaign = $this->mc()->get( '/campaigns/'. $content->campaign_id);
      
      if( (array_key_exists( 'status', $campaign) && $campaign ['status'] == 404))
      {
        $campaign = $this->createCampaign( $content);
      }
    }

    $this->CrudTool->addSerialized([
      'campaign' => $campaign
    ]);


    $this->_updateCampaign( $content);
  }

  protected function _updateCampaign( $content)
  {
    $result = $this->mc()->put( '/campaigns/'. $content->campaign_id . '/content', [
      'url' => \Cake\Routing\Router::url('/'. strtolower( $this->request->getParam( 'plugin')) .'/newsletters/index/'. $content->id, true)
    ]);
  }
}