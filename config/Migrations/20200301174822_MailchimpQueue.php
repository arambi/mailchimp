<?php
use Migrations\AbstractMigration;

class MailchimpQueue extends AbstractMigration
{
  public function change()
  {
    $queue = $this->table( 'mailchimp_mailchimp_queue');
    $queue
      ->addColumn( 'url', 'string', ['limit' => 50, 'null' => true, 'default' => NULL])
      ->addColumn( 'method', 'string', ['limit' => 8, 'null' => true, 'default' => NULL])
      ->addColumn( 'data', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'error', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'readed', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'readed_at', 'datetime', ['null' => true, 'default' => NULL])
      ->addColumn( 'created', 'datetime', ['null' => true, 'default' => NULL])
      ->addColumn( 'modified', 'datetime', ['null' => true, 'default' => NULL])
      ->addIndex( 'readed')
      ->create();
  }
}
