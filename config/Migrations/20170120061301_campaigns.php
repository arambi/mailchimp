<?php

use Phinx\Migration\AbstractMigration;

class Campaigns extends AbstractMigration
{

  public function change()
  {
    // Tabla para los models de contenido
    $campaigns = $this->table( 'mailchimp_campaigns');
    $campaigns
      ->addColumn( 'campaign_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'template_id', 'integer', ['null' => true, 'default' => NULL])
      ->addColumn( 'body', 'text', ['null' => true, 'default' => NULL])
      ->addColumn( 'schedule_time', 'datetime', ['default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['campaign_id'])
      ->addIndex( ['template_id'])
      ->save();
  }
}
