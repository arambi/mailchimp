<?php
namespace Mailchimp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cities Model
 *
 * @method \Geroabai\Model\Entity\City get($primaryKey, $options = [])
 * @method \Geroabai\Model\Entity\City newEntity($data = null, array $options = [])
 * @method \Geroabai\Model\Entity\City[] newEntities(array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Geroabai\Model\Entity\City patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City[] patchEntities($entities, array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ListsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( false);

    $this->alias( 'Lists');
    $this->displayField( 'name');

    // Behaviors
    $this->addBehavior( 'Manager.Crudable');

    // CRUD Config
    $this->crud
      ->addFields([
        'name' => [
          'type' => 'string',
          'label' => 'Nombre'
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'name'
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Listas'),
        'plural' => __d( 'admin', 'Listas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => []
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }

}
