<?php

namespace Mailchimp\Traits;

use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use DrewM\MailChimp\MailChimp;


trait MailchimpTrait
{
    protected $_api;

    public function mc()
    {
        if (!Website::get('settings.mailchimp')) {
            $site = TableRegistry::getTableLocator()->get('Website.Sites')->find()->first();
            Website::set($site);
        }
        
        if (empty($_api)) {
            $this->_api = new MailChimp(Website::get('settings.mailchimp'));
        }

        return $this->_api;
    }
}
