<?php
namespace Mailchimp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MailchimpQueueTable extends Table
{
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('mailchimp_mailchimp_queue');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');
    $this->addBehavior('Timestamp');

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => ['data', 'error']
    ]);
  }
}
