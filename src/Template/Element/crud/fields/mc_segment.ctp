<div class="form-group" ng-if="data.segments">
  <label ng-help tooltip="{{ field.help }}" class="control-label">{{ field.label }}</label>
  <div>
  <select class="form-control" ng-model="data.content.segment_id">
    <option value="" ng-selected="!data.content.segment_id">-- Ninguno --</option>
    <option ng-selected="id == data.content.segment_id" ng-repeat="(id, title) in data.segments" value="{{ id }}">{{ title }}</option>
  </select>
  </div>
</div>
