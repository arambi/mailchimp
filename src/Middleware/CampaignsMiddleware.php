<?php

namespace Mailchimp\Middleware;

use Cake\Core\Configure;
use Mailchimp\Traits\MailchimpTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Campaigns middleware
 */
class CampaignsMiddleware
{
    use MailchimpTrait;

    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        /** @var \Cake\Http\ServerRequest $request */
        if ($request->getQuery('mc_cid')) {
            $request->getSession()->write('Mailchimp.campaign_id', $request->getQuery('mc_cid'));
        }

        if ($request->getQuery('mc_eid')) {
            if ($member = $this->getMember($request->getQuery('mc_eid'))) {
                $request->getSession()->write('Mailchimp.campaign_member_email', $member['email_address']);
            }
        }

        if (!empty($_SESSION['Mailchimp']['campaign_id'])) {
            Configure::write('Mailchimp.campaign_id', $_SESSION['Mailchimp']['campaign_id']);
        }

        if (!empty($_SESSION['Mailchimp']['campaign_member_email'])) {
            Configure::write('Mailchimp.campaign_member_email', $_SESSION['Mailchimp']['campaign_member_email']);
        }

        return $next($request, $response);
    }

    private function writeConfigure($request)
    {
        

        return $request;
    }

    private function getMember($id)
    {
        try {
            $lists = $this->mc()->get( 'lists');
            
            foreach ($lists['lists'] as $list) {
                $member = $this->mc()->get("/lists/{$list['id']}/members", [
                    'unique_email_id' => $id
                ]);

                if ($member && isset($member['members'][0])) {
                    return $member['members'][0];
                }
            }
        } catch (\Throwable $th) {
        }
    }
}
