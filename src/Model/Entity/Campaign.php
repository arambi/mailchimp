<?php
namespace Mailchimp\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Store Entity
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $address
 * @property string $postcode
 * @property string $city
 * @property string $status
 * @property int $category_id
 * @property float $lat
 * @property float $lng
 * @property int $zoom
 * @property bool $published
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Campaign extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
  ];

}
