<?php

namespace Mailchimp\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Mailchimp\Traits\MailchimpTrait;

/**
 * Cities Model
 *
 * @method \Geroabai\Model\Entity\City get($primaryKey, $options = [])
 * @method \Geroabai\Model\Entity\City newEntity($data = null, array $options = [])
 * @method \Geroabai\Model\Entity\City[] newEntities(array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Geroabai\Model\Entity\City patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City[] patchEntities($entities, array $data, array $options = [])
 * @method \Geroabai\Model\Entity\City findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CampaignsTable extends Table
{
  use MailchimpTrait;

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('mailchimp_campaigns');

    $this->alias('Campaigns');
    $this->displayField('name');

    // Behaviors
    $this->addBehavior('Timestamp');
    $this->addBehavior('Manager.Crudable');

    // CRUD Config
    $this->crud
      ->addFields([
        'type' => [
          'type' => 'select',
          'label' => __d('admin', 'Tipo de campaña'),
          'options' => [
            'regular' => __d('admin', 'Regular'),
            'plaintext' => __d('admin', 'Texto plano'),
            'rss' => __d('admin', 'RSS'),
          ]
        ],
        'settings.subject_line' => [
          'type' => 'string',
          'label' => __d('admin', 'Tema del correo'),
          'help' => __d('admin', 'El tema del correo para la campaña'),
        ],
        'settings.title' => [
          'type' => 'string',
          'label' => __d('admin', 'Título'),
          'help' => __d('admin', 'El título de la campaña'),
        ],
        'settings.from_name' => [
          'type' => 'string',
          'label' => __d('admin', 'Nombre del remitente'),
          'help' => __d('admin', 'El nombre del remitente para la campaña'),
        ],
        'settings.reply_to' => [
          'type' => 'string',
          'label' => __d('admin', 'Responder a este email'),
          'help' => __d('admin', 'Dirección de correo a la que podrán responder los destinatarios de la campaña'),
        ],
        'recipients.list_id' => [
          'type' => 'select',
          'options' => function ($crud) {
            $account = $this->mc()->get('/');
            $lists = $this->mc()->get('lists', ['count' => 100]);
            return collection($lists['lists'])->combine('id', 'name');
          },
          'label' => __d('admin', 'Lista de correo'),
          'help' => __d('admin', 'Selecciona una lista de usuarios'),
        ],
        'template_id' => [
          'type' => 'select',
          'options' => function ($crud) {
            $account = $this->mc()->get('/');
            $templates = $this->mc()->get('templates', ['count' => 100]);

            $return = [];

            foreach (current($templates) as $template) {
              if (!empty($template['created_by'])) {
                $return[$template['id']] = $template['name'];
              }
            }

            return $return;
          },
          'label' => __d('admin', 'Plantilla'),
          'help' => __d('admin', 'Selecciona una plantilla'),
        ],
        'body' => [
          'type' => 'ckeditor',
          'label' => __d('admin', 'Cuerpo de texto'),
          'help' => __d('admin', 'Dirección de correo a la que podrán responder los destinatarios de la campaña'),
        ],
        'send' => [
          'type' => 'boolean',
          'label' => __d('admin', 'Realizar el envío'),
          'help' => __d('admin', 'Al marcar esta opción y guardar el contenido se pondrá en marcha el envío para la fecha y hora indicadas'),
          'show' => '(content.status == "save" || content.status == "paused") && content.template_id && content.schedule_time && content.recipients.list_id'
        ],
        'schedule_time' => [
          'type' => 'datetime',
          'label' => __d('admin', 'Fecha y hora del envío'),
          'show' => 'content.status == "save"',
          'help' => __d('admin', 'La fecha y hora del envío'),
          'minuteStep' => 15,
        ],
        'send_time' => [
          'type' => 'datetime',
          'label' => __d('admin', 'Fecha y hora del envío'),
          'show' => 'content.send',
          'show' => 'content.status == "save"'
        ],
        'schedule_info' => [
          'type' => 'info',
          'template' => 'Mailchimp.fields/mc_schedule_info',
        ],
        'paused' => [
          'type' => 'boolean',
          'label' => 'Parar envío',
          'show' => 'content.status == "schedule"'
        ],
        'status' => [
          'type' => 'select',
          'options' => [
            'sent' => '<span class="badge badge-primary">' . __d('admin', 'Enviado') . '</span>',
            'paused' => '<span class="badge badge-warning">' . __d('admin', 'Pausado') . '</span>',
            'save' => '<span class="badge badge-success">' . __d('admin', 'Editándose') . '</span>',
            'schedule' => '<span class="badge badge-info">' . __d('admin', 'Listo para enviar') . '</span>',
            'sending' => '<span class="badge badge-danger">' . __d('admin', 'Enviándose') . '</span>',
          ]
        ],
      ])
      ->addIndex('index', [
        'fields' => [
          'settings.title',
          'status',
          'send_time'
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
        'noSearch' => true
      ])
      ->setName([
        'singular' => __d('admin', 'Campaña'),
        'plural' => __d('admin', 'Campañas'),
      ])
      ->addView('create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  // 'type',
                  'settings.title',
                ]
              ],
              [
                'title' => __d('admin', 'Envío'),
                'elements' => [
                  'send',
                  'schedule_time',
                  'schedule_info',
                  'paused'
                ]
              ],
              [
                'title' => __d('admin', 'Datos del envío'),
                'elements' => [
                  'settings.subject_line',
                  'settings.from_name',
                  'settings.reply_to',
                ]
              ],
              [
                'title' => __d('admin', 'Lista de correo'),
                'elements' => [
                  'recipients.list_id',
                ]
              ],
              [
                'title' => __d('admin', 'Contenido'),
                'elements' => [
                  'template_id',
                  'body'
                ]
              ],

            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ->defaults([
        'schedule_time' => date('Y-m-d H:i', strtotime('+1 day')),
      ])
      ;
      
  }


  public function readCampaign($id)
  {
    $campaign = $this->mc()->get('campaigns/' . $id);
    $content = $this->mc()->get('campaigns/' . $id . '/content', [
      'fields' => [
        'template'
      ]
    ]);

    $entity = $this->getEntity($id);

    if (!$content) {
      $content = [];
    }

    $campaign['content'] = $content;
    $campaign = $this->setEntity($campaign, $entity);

    return $campaign;
  }

  public function setAction($id, $campaign, $data, $entity)
  {
    if ($campaign['status'] == 'save' && @$data['send'] == 1 && !empty(@$data['schedule_time'])) {
      $response = $this->mc()->post('campaigns/' . $id . '/actions/schedule', [
        'schedule_time' => $data['schedule_time']
      ]);

      if (is_array($response) && array_key_exists('status', $response)) {
        $entity->errors('send', [__d('admin', 'La campaña no ha podido enviarse. Es posible que la fecha no sea correcta o el correo de respuesta no esté validado.')]);
      }
    }

    if ($campaign['status'] == 'schedule' && !empty(@$data['paused'])) {
      $response = $this->mc()->post('campaigns/' . $id . '/actions/unschedule');
    }
  }

  public function createCampaign($data)
  {
    $campaign = $this->mc()->post('campaigns', $data);

    if ($campaign['status'] == 400) {
      return $campaign;
    }

    $id = $campaign['id'];

    $_content = $this->mc()->put('campaigns/' . $id . '/content', [
      'template' => [
        'id' => @$data['template_id'],
        'sections' => [
          'body' => $this->setDomain($data['body'])
        ]
      ]
    ]);

    $entity = $this->saveEntity($id, $data);
    $this->setAction($id, $campaign, $data, $entity);
    $campaign = $this->setEntity($campaign, $entity);

    return $campaign;
  }


  public function updateCampaign($id, $data)
  {
    $campaign = $this->mc()->patch('campaigns/' . $id, $data);
    $campaign = $this->mc()->get('campaigns/' . $id);
    $_content = $this->mc()->put('campaigns/' . $id . '/content', [
      'template' => [
        'id' => $data['template_id'],
        'sections' => [
          'body' => $this->setDomain($data['body'])
        ]
      ]
    ]);


    $entity = $this->saveEntity($id, $data);
    $this->setAction($id, $campaign, $data, $entity);
    $campaign = $this->setEntity($campaign, $entity);

    return $entity;
  }

  public function setEntity($campaign, $entity)
  {
    $campaign['template_id'] = $entity->template_id;
    $campaign['body'] = $entity->body;
    $campaign['schedule_time'] = $entity->schedule_time;
    $campaign['send'] = $entity->send;
    return $campaign;
  }


  public function getEntity($campaign_id)
  {
    $entity = $this->find()
      ->where([
        'campaign_id' => $campaign_id
      ])
      ->first();

    if (!$entity) {
      $entity = $this->createEntity($campaign_id);
    }

    return $entity;
  }

  public function createEntity($campaign_id, $data = [])
  {
    $data['campaign_id'] = $campaign_id;
    return $this->save($this->newEntity($data));
  }

  public function saveEntity($id, $data)
  {
    $entity = $this->getEntity($id);
    $entity->set('template_id', @$data['template_id']);
    $entity->set('body', $data['body']);
    $entity->set('send', @$data['send']);
    if (empty($data['schedule_time'])) {
      $entity->set('schedule_time', @date('Y-m-d H:i:s', strtotime('+5 hours')));
    } else {
      $entity->set('schedule_time', @date('Y-m-d H:i:s', strtotime($data['schedule_time'])));
    }

    return $this->save($entity);
  }

  public function setDomain($body)
  {
    $body = str_replace('src="/', 'src="http://' . env("SERVER_NAME") . '/', $body);
    return $body;
  }

  public function setErrors($entity, $errors)
  {
    $messages = [
      'Subject line missing' => __d('admin', 'Es necesario indicar un tema'),
      'From line missing' => __d('admin', 'Es necesario indicar un remitente'),
      'Reply to missing' => __d('admin', 'Es necesario indicar la dirección de correo'),
      'Required fields were not provided: settings' => __d('admin', 'Es necesario indicar un título'),
    ];

    $_errors = [];

    foreach ($errors as $error) {
      if (array_key_exists($error['message'], $messages)) {
        $_errors[] = $messages[$error['message']];
      }
    }

    $entity->errors('settings.subject_line', $_errors);
  }
}
