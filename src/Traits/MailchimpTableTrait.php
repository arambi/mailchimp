<?php 

namespace Mailchimp\Traits;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

trait MailchimpTableTrait
{
  public function addMcFields()
  {
    $this->crud->addFields([
      'mc_preview' => [
        'type' => 'info',
        'template' => 'Mailchimp.fields/mc_preview'
      ],
      'list_id' => [
        'type' => 'select',
        'options' => function( $crud){
          $account = $this->mc()->get( '/');
          $lists = $this->mc()->get( 'lists');
          return collection( $lists ['lists'])->combine( 'id', 'name');
        },
        'label' => __d( 'admin', 'Lista de correo'),
        'help' => __d( 'admin', 'Selecciona una lista de usuarios'),
      ],
    ]);
  }

  public function afterDelete( Event $event, EntityInterface $entity)
  {
    if( !empty( $entity->campaign_id))
    {
      $this->mc()->delete( 'campaigns/'. $entity->campaign_id);
    }
  }
}