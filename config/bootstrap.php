<?php 

use User\Auth\Access;
use Cake\Event\EventManager;
use Block\Lib\BlocksRegistry;
use Section\Action\ActionCollection;
use Mailchimp\Event\MailchimpListener;
use Manager\Navigation\NavigationCollection;
 

EventManager::instance()->attach( new MailchimpListener());

Access::add( 'mailchimp', [
  'name' => 'Mailchimp',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Lists',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Lists',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Lists',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Lists',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Lists',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Campaigns',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Campaigns',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Campaigns',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Campaigns',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Campaigns',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Mailchimp',
  'icon' => 'fa fa-envelope',
  // 'url' => false,
  'key' => 'mailchimp',
  'plugin' => 'Mailchimp',
  'controller' => 'Campaigns',
  'action' => 'index',
]);

// NavigationCollection::add( [
//   'parent' => 'mailchimp',
//   'name' => 'Listas',
//   'parentName' => 'Mailchimp',
//   'plugin' => 'Mailchimp',
//   'controller' => 'Lists',
//   'action' => 'index',
//   'icon' => 'fa fa-list',
// ]);
// NavigationCollection::add( [
//   'parent' => 'mailchimp',
//   'name' => 'Campañas',
//   'parentName' => 'Mailchimp',
//   'plugin' => 'Mailchimp',
//   'controller' => 'Campaigns',
//   'action' => 'index',
//   'icon' => 'fa fa-list',
// ]);




Access::add( 'newsletters', [
  'name' => 'Newsletters',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Mailchimp',
          'controller' => 'Newsletters',
          'action' => 'fields',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Newsletters',
  'parentName' => 'Newsletters',
  'plugin' => 'Mailchimp',
  'controller' => 'Newsletters',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);









// Bloque Texto
BlocksRegistry::add( 'text_mc', [
    'key' => 'text_mc',
    'title' => __d( 'admin', 'Texto MC'),
    'icon' => 'fa fa-square-o',
    'afterAddTarget' => 'parent',
    'inline' => true,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Mailchimp\\Model\\Block\\TextMcBlock',
    'cell' => 'Mailchimp.TextMc::display',
    'blockView' => 'Mailchimp/blocks/text_mc',
    'disableByDefault' => true
]);
