<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class=" control-label">{{ field.label }}</label>
  <div>
  <input 
    cf-bind-model="field.key" 
    ng-class="{inputbig: field.key == data.crudConfig.model.displayField}" 
    type="text" class="form-control">
  
  <div style="margin-top: 5px; margin-bottom: 10px" ng-show="data.content.list_id">
    <span class="form-error" ng-if="field.error" ng-repeat="(key, error) in field.error">{{ error }}</span>
    Inserta un campo 
    <select style="margin-left: 5px" mc-insert-field>
      <option value="">-- Selecciona --</option>
      <option ng-repeat="mc_field in data.merge_fields" value="{{ mc_field.tag }}">{{ mc_field.name }}</option>
    </select>
  </div>
  </div>
</div>
