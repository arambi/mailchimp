<?php
namespace Mailchimp\Shell;

use Cake\Console\Shell;
use Mailchimp\Traits\MailchimpTrait;

/**
 * MailchimpSender shell command.
 */
class MailchimpSenderShell extends Shell
{
  use MailchimpTrait;
  
  public function main()
  {
    $actions = $this->findActions();

    foreach( $actions as $action)
    {
      try {
        $response = $this->mc()->{$action->method}( $action->url, (array)$action->data);

        if( empty( $response ['errors']))
        {
          $this->markReaded( $action->id);
        }
        else
        {
          $this->markError( $action->id, $response);
        }
      } catch (\Throwable $th) {
        \Cake\Log\Log::debug( $th);
      }
    }
  }

  public function test()
  {
    $response = $this->mc()->get('/ecommerce/stores/store/customers');
  }

  private function findActions()
  {
    $this->loadModel( 'Mailchimp.MailchimpQueue');

    $actions = $this->MailchimpQueue->find()
      ->where([
        'MailchimpQueue.readed' => false
      ])
      ->order([
        'MailchimpQueue.created' => 'asc'
      ])
    ;

    return $actions;
  }

  private function markReaded( $id)
  {
    $this->loadModel( 'Mailchimp.MailchimpQueue')->query()->update()
      ->set([
        'readed' => true,
        'readed_at' => date( 'Y-m-d H:i:s')
      ])
      ->where([
        'id' => $id
      ])
      ->execute();
  }

  private function markError( $id, $response)
  {
    $this->loadModel( 'Mailchimp.MailchimpQueue')->query()->update()
      ->set([
        'error' => $response
      ])
      ->where([
        'id' => $id
      ])
      ->execute();
  }
}
