<?php 

namespace Mailchimp\Queue;

use Cake\ORM\TableRegistry;

class MailchimpQueue
{
  public static function getTable()
  {
    return TableRegistry::getTableLocator()->get( 'Mailchimp.MailchimpQueue');
  }
  
  public static function add( $url, $method, $data = [])
  {
    $table = static::getTable();
    $entity = $table->newEntity([
      'url' => $url,
      'method' => $method,
      'data' => $data
    ]);

    $table->save( $entity);
  }
}