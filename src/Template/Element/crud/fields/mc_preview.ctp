<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class=" control-label"></label>
  <div>
    <a href="{{ data.content.campaign.long_archive_url }}" target="_blank" class="btn btn-primary btn-sm m-t "><?= __d( 'admin', 'Previsualizar') ?></a> 
  </div>
</div>