<?php
namespace Mailchimp\Controller\Admin;

use Cake\ORM\Entity;
use Mailchimp\Datasource\Paginator;
use Mailchimp\Model\Entity\Campaign;
use Mailchimp\Traits\MailchimpTrait;
use Mailchimp\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Lists Controller
 *
 * @property \Mailchimp\Model\Table\ListsTable $Lists
 */
class CampaignsController extends AppController
{
  use CrudControllerTrait {
    initialize as initializeCrud;
  }

  use MailchimpTrait;

  public function initialize() 
  {
    $this->initializeCrud();
  }
   
  public function index()
  { 
    $limit = 20;

    $page = $this->getRequest()->getQuery( 'page');

    if( !$page)
    {
      $page = 1;
    }

    $offset = ($page * $limit) - $limit;
    
    $this->loadComponent( 'Mailchimp.Paginator');

    $campaigns = $this->mc()->get( 'campaigns', [
      'count' => $limit,
      'offset' => $offset,
      'sort_dir' => 'desc',
      'sort_field' => 'create_time',
    ]);

    $this->CrudTool->addSerialized( [
      'contents' => $campaigns ['campaigns']
    ]);

    $this->Paginator->setMailchimpPaginator( new Paginator());

    $paging = [
      'Campaigns' => $this->Paginator->getPaginator()->pagination( 'Campaings', $campaigns ['total_items'], 20, [
        'page' => $page
      ])
    ];

    $this->getRequest()->addParams( compact( 'paging'));

  }

  public function create()
  {
    if( $this->request->is( ['patch', 'post', 'put']))
    {
      $this->request->data ['type'] = 'regular';
      $content = $this->Campaigns->createCampaign( $this->request->data);

      if( $content ['status'] == 400)
      {
        $entity = $this->Table->newEntity( $this->request->data);
        $this->Table->setErrors( $entity, $content ['errors']);
        $this->Table->crud->setEntity( $entity);
        $this->Table->crud->setContent( $this->Table->emptyEntityProperties( $entity));
      }
      else
      {
        $this->update( $content ['id'], true);        
      }
    }
    else
    {
      $this->Table->crud->setContent( $this->Table->emptyEntityProperties( new Campaign( $this->request->data)));
    }
  }

  public function update( $id = false, $created = false)
  {
    $this->CrudTool->setAction( 'update');
    $_id = $id ? $id : ($this->request->data( 'id') ? $this->request->data( 'id') : false);
    $content = $this->Campaigns->readCampaign( $_id);

    if( !$created && $this->request->is( ['patch', 'post', 'put']))
    {
      $entity = $this->Campaigns->updateCampaign( $_id, $this->request->data);
      $content = $this->Campaigns->readCampaign( $_id);
    }

    if( !isset( $entity))
    {
      $entity = new Campaign( $content);
    }
    
    $this->Table->crud->setEntity( $entity);
    $this->Table->crud->setContent( $content);
  }

  public function delete()
  {
    if( !$this->request->data( 'id'))
    {
      throw new \Cake\Network\Exception\NotFoundException( __d( 'admin', 'No ha sido posible borrar el contenido'));
    }

    $result = $this->mc()->delete( 'campaigns/' . $this->request->data( 'id'));

    if( $result)
    {
      $this->CrudTool->addSerialized([
        'isDeleted' => true,
      ]);
    }
    else
    {
      $this->CrudTool->addSerialized([
        'isDeleted' => false,
        'deleteErrors' => $result ['title']
      ]);
    }
  }
}
