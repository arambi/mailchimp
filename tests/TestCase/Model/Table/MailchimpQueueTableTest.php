<?php
namespace Mailchimp\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Mailchimp\Model\Table\MailchimpQueueTable;

/**
 * Mailchimp\Model\Table\MailchimpQueueTable Test Case
 */
class MailchimpQueueTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Mailchimp\Model\Table\MailchimpQueueTable
     */
    public $MailchimpQueue;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Mailchimp.MailchimpQueue',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MailchimpQueue') ? [] : ['className' => MailchimpQueueTable::class];
        $this->MailchimpQueue = TableRegistry::getTableLocator()->get('MailchimpQueue', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MailchimpQueue);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
