<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         2.0.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace Mailchimp\Controller\Component;

use InvalidArgumentException;
use Cake\Controller\Component;
use Mailchimp\Datasource\Paginator;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Exception\NotFoundException;
use Cake\Controller\Component\PaginatorComponent as PaginatorComponentOriginal;
use Cake\Datasource\Exception\PageOutOfBoundsException;

/**
 * This component is used to handle automatic model data pagination. The primary way to use this
 * component is to call the paginate() method. There is a convenience wrapper on Controller as well.
 *
 * ### Configuring pagination
 *
 * You configure pagination when calling paginate(). See that method for more details.
 *
 * @link https://book.cakephp.org/3.0/en/controllers/components/pagination.html
 */
class PaginatorComponent extends PaginatorComponentOriginal
{

    /**
     * Set paginator instance.
     *
     * @param \Cake\Datasource\Paginator $paginator Paginator instance.
     * @return self
     */
    public function setMailchimpPaginator(Paginator $paginator)
    {
        $this->_paginator = $paginator;

        return $this;
    }

   
}
