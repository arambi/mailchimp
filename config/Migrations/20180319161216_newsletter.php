<?php
use Migrations\AbstractMigration;

class Newsletter extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    // Tabla para los models de contenido
    $campaigns = $this->table( 'mailchimp_newsletters');
    $campaigns
      ->addColumn( 'title', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => NULL, 'limit' => 5])
      ->addColumn( 'campaign_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'template_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'list_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 64])
      ->addColumn( 'segment_id', 'integer', ['null' => true, 'default' => NULL])
      ->addColumn( '_match', 'string', ['null' => true, 'default' => NULL, 'limit' => 16])
      ->addColumn( 'schedule_time', 'datetime', ['null' => true, 'default' => null])
      ->addColumn( 'settings', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'conditions', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['campaign_id'])
      ->addIndex( ['template_id'])
      ->addIndex( ['list_id'])
      ->addIndex( ['schedule_time'])
      ->create();
  }
}
