;(function(){
  function mcAddCondition( $timeout) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          if( !scope.data.content.conditions){
            scope.data.content.conditions = [];
          }

          scope.data.content.conditions.push({
          });
          $timeout(function(){
            scope.$apply();
          })
        })
      }
    }
  }


function mcInsertField( $timeout) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        $(element).change(function(){
          scope.data.content.title = scope.data.content.title + ' *|' + $(this).val() + '|*'; 
          scope.$apply();
        })
      }
    }
  }

  

  /**
   *
   * Pass all functions into module
   */
  angular
      .module('admin')
      .directive( 'mcAddCondition', mcAddCondition)
      .directive( 'mcInsertField', mcInsertField)

  ;
})()

  