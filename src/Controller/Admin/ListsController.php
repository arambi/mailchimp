<?php
namespace Mailchimp\Controller\Admin;

use Mailchimp\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Lists Controller
 *
 * @property \Mailchimp\Model\Table\ListsTable $Lists
 */
class ListsController extends AppController
{
  use CrudControllerTrait {
    initialize as initializeCrud;
  }

  public function initialize() 
  {
    $this->initializeCrud();
    $this->loadComponent( 'Mailchimp.Mailchimp');
  }
   
  public function index()
  { 
    $contents = $this->Mailchimp->lists();

    $this->CrudTool->addSerialized( [
      'contents' => $contents
    ]);
  }

  public function update( $id)
  {
    $contents = $this->Mailchimp->api->get( 'lists/' . $id . '/members');
    _d( $contents);
  }
}
