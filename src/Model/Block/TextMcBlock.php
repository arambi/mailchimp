<?php

namespace Mailchimp\Model\Block;

class TextMcBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Texto'),
          'plural' => __d( 'admin', 'Texto'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}