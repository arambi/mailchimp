<?php
use Migrations\AbstractMigration;

class NewslettersSegmentString extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    // Tabla para los models de contenido
    $campaigns = $this->table( 'mailchimp_newsletters');

    if( !$campaigns->hasColumn( 'segment_id'))
    {
      $campaigns
        ->addColumn( 'segment_id', 'string', ['null' => true, 'default' => NULL])
        ->update();
    }
  }
}
