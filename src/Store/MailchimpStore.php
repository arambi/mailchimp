<?php 

namespace Mailchimp\Store;

use Cake\Core\Configure;

class MailchimpStore
{
  public static function getMergeFields( $list_id)
  {
    if( empty( $list_id))
    {
      return [];
    }

    $lists = (array)Configure::read( 'Mailchimp.lists');

    if( array_key_exists( $list_id, $lists))
    {
      return $lists [$list_id];
    }

    $mc = new \DrewM\MailChimp\MailChimp( \Website\Lib\Website::get( 'settings.mailchimp'));
    $list = $mc->get( 'lists/'. $list_id .'/merge-fields');

    if( empty( $list ['merge_fields']))
    {
      return [];
    }
    
    $merge_fields = collection( $list ['merge_fields'])->combine( 'tag', 'name')->toArray();
    $lists [$list_id] = $merge_fields;
    Configure::write( 'Mailchimp.lists', $lists);
    return $merge_fields;
  }
}