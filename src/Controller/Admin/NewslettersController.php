<?php
namespace Mailchimp\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Mailchimp\Controller\AppController;
use Mailchimp\Traits\MailchimpTrait;
use Website\Lib\Website;
use Manager\Crud\Flash;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Manager\FieldAdapter\FieldAdapterRegistry;
use Cake\I18n\Time;
/**
 * Newsletters Controller
 *
 * @property \Mailchimp\Model\Table\NewslettersTable $Newsletters
 */
class NewslettersController extends AppController
{
  use CrudControllerTrait {
    initialize as initializeCrud;
  }

  use MailchimpTrait;

  private $conditionTypes = [
    'birthday' => 'BirthdayMerge',
    'date' => 'DateMerge',
    'text' => 'TextMerge',
    'dropdown' => 'SelectMerge',
    'radio' => 'SelectMerge',
  ];

  private $mergeFields;

  public function initialize() 
  {
    $this->initializeCrud();
  }

  private function setCkeditor()
  {
    $ckeditor = FieldAdapterRegistry::get( 'block');
    $config = $ckeditor->getConfig( 'default');
    $config ['extraPlugins'] .= ',strinsert';
    $config ['toolbar'][] = [
      'name' => 'inserts',
      'items' => ['strinsert']
    ];

    if( $this->mergeFields && is_array( $this->mergeFields) && array_key_exists( 'merge_fields', $this->mergeFields))
    {
      foreach( $this->mergeFields ['merge_fields'] as $field)
      {
        $combo [] = [
          '*|'. $field ['tag'] . '|*',
          $field ['name'],
          $field ['name'],
        ];
      }

      $config ['strinsert_strings'] = $combo;
      $config ['strinsert_title'] = 'Campos Mailchimp';
    }

    $ckeditor->addConfig( 'mailchimp', $config);
    $ckeditor->set( 'mailchimp', 'Newsletters', 'blocks');
  }

  protected function _beforeUpdateFind( $query, $content)
  {
    // if( !$this->request->is( ['patch', 'post', 'put']))
    // {
      $query->formatResults( function( $results){
        return $results->map( function( $row){
          $row = $this->setCampaign( $row);
          return $row;
        });
      });
    // }

    $this->_setFields( $content->list_id);
    $this->setCkeditor();
  }

  
  protected function _beforeCreate( $data)
  {
    $this->setConditionsOptions();
  }
  
  protected function _beforeUpdate( $id, $data)
  {
    $this->setConditionsOptions();
  }

  public function fields()
  {
    $this->_setFields( $this->request->getData( 'list_id'));
  }

  private function _getMergeFields( $list_id)
  {
    if( $this->mergeFields)
    {
      return $this->mergeFields;
    }

    $fields = $this->mc()->get( '/lists/'. $list_id .'/merge-fields', [
      'count' => 50
    ]);
    $this->mergeFields = $fields;
    return $fields;
  }

  protected function _setFields( $list_id)
  {
    $fields = $this->_getMergeFields( $list_id);
    $merge_fields = [];
    $merge_fields_dropdown_options = [];

    if( array_key_exists( 'merge_fields', $fields))
    {
      foreach( $fields ['merge_fields'] as $field)
      {
        $merge_fields [$field ['tag']] = $field;

        if( in_array( $field ['type'], ['dropdown', 'radio']))
        {
          $merge_fields_dropdown_options [ $field ['tag']] = $field ['options']['choices'];
        }
      }
    }
    else
    {
      $merge_fields = [];
    }

    // segments
    $results = $this->mc()->get( '/lists/'. $list_id .'/segments', [
      'count' => 1000
    ]);
  
    $segments = [
      'custom' => 'Condiciones propias'
    ];

    if( array_key_exists( 'segments', $results))
    {
      foreach( $results ['segments'] as $segment)
      {
        $segments [$segment ['id']] = $segment ['name'];
      }
    }

    $this->set( compact( 'merge_fields', 'merge_fields_dropdown_options', 'segments'));
  }

  private function setConditionsOptions()
  {
    $mcFieldOptions = [
      'text' => [
        'is' => __d( 'admin', 'es'),
        'not' => __d( 'admin', 'no es'),
        'contains' => __d( 'admin', 'contiene'),
        'notcontain' => __d( 'admin', 'no contiene'),
        'starts' => __d( 'admin', 'comienza por'),
        'ends' => __d( 'admin', 'termina por'),
        'greater' => __d( 'admin', 'es mayor que'),
        'less' => __d( 'admin', 'es menor que'),
        'blank' => __d( 'admin', 'está vacío'),
        'blank_not' => __d( 'admin', 'no está vacío'),
      ],
      'birthday' => [
        'is' => __d( 'admin', 'es'),
        'not' => __d( 'admin', 'no es'),
        'blank' => __d( 'admin', 'está vacío'),
        'blank_not' => __d( 'admin', 'no está vacío'),
      ],
      'dropdown' => [
        'is' => __d( 'admin', 'es'),
        'not' => __d( 'admin', 'no es'),
        'blank' => __d( 'admin', 'está vacío'),
        'blank_not' => __d( 'admin', 'no está vacío'),
      ],
      'radio' => [
        'is' => __d( 'admin', 'es'),
        'not' => __d( 'admin', 'no es'),
        'blank' => __d( 'admin', 'está vacío'),
        'blank_not' => __d( 'admin', 'no está vacío'),
      ],
    ];

    $this->set( compact( 'mcFieldOptions'));
  }

  protected function _afterSave( $content)
  {
    if( empty( $content->campaign_id))
    {
      $campaign = $this->createCampaign( $content);
      $campaign_id = $campaign ['id'];
    }
    else
    {
      $campaign_id = $content->campaign_id;
      $result = $this->mc()->patch( '/campaigns/'. $campaign_id, $content->mcOptions());
      $this->_updateCampaign( $content);
    }

    if( !empty( $content->schedule_time))
    {
      $time = is_object( $content->schedule_time) ? $content->schedule_time->format( 'Y-m-dTH:i:s') : (new Time( strtotime( $content->schedule_time . ' -1 hour' )))->format( 'Y-m-dTH:i:s');
      $this->mc()->post( '/campaigns/'. $campaign_id . '/actions/unschedule');
      $result = $this->mc()->post( '/campaigns/'. $campaign_id . '/actions/schedule', [
        'schedule_time' => $time,
      ]);
    
      \Cake\Log\Log::debug( $result);

      if( $result ['status'] == 400)
      {
        Flash::error( 'Ha habido un error en Mailchimp y el envío no ha podido ser programado (' . $result ['detail'] . ')');
      }
    }
  }

  private function createCampaign( $content)
  {   
    // Crea campaña
    $campaign = $this->mc()->post( '/campaigns', $content->mcOptions());
    $this->Table->query()->update()
      ->set(['campaign_id' => $campaign ['id']])
      ->where(['id' => $content->id])
      ->execute();
    
    $content->set( 'campaign_id', $campaign ['id']);
    $this->_updateCampaign( $content);

    return $campaign;
  }

  protected function _updateCampaign( $content)
  {
    if( Configure::read( 'App.mailchimpLocal'))
    {
      return;
    }

    if (!Configure::read( 'I18n.disable')) {
      $url = Router::url('/'. $content->locale .'/mailchimp/newsletters/index/'. $content->id, true);
    } else {
      $url = Router::url('/mailchimp/newsletters/index/'. $content->id, true);
    }

    $result = $this->mc()->put( '/campaigns/'. $content->campaign_id . '/content', [
      'url' => $url
    ]);
  }

  private function setCampaign( $row)
  {
    if( !empty( $row ['campaign']['recipients']['segment_opts']['conditions']))
    {
      $conditions = [];

      foreach( $row ['campaign']['recipients']['segment_opts']['conditions'] as $condition)
      {
        $condition ['type'] = array_search( $condition ['condition_type'], $this->conditionTypes);
        unset( $condition ['condition_type']);
        $conditions [] = $condition;
      }

      $row ['conditions'] = $conditions;
    }

    if( !empty( $row ['campaign']['recipients']['segment_opts']['match']))
    {
      $row ['_match'] = $row ['campaign']['recipients']['segment_opts']['match'];
    }   

    if( !empty( $row ['campaign']['recipients']['segment_opts']['saved_segment_id']))
    {
      $row ['segment_id'] = $row ['campaign']['recipients']['segment_opts']['saved_segment_id'];
      $row ['conditions'] = [];
    }   
    elseif( !empty( $row ['campaign']['recipients']['segment_opts']['conditions']))
    {
      $row ['segment_id'] = 'custom';
    }
    
    return $row;
  }
}
