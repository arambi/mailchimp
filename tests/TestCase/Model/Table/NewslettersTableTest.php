<?php
namespace Mailchimp\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Mailchimp\Model\Table\NewslettersTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Mailchimp\Model\Table\NewslettersTable Test Case
 */
class NewslettersTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Mailchimp\Model\Table\NewslettersTable
     */
    public $Newsletters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.mailchimp.newsletters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Newsletters') ? [] : ['className' => NewslettersTable::class];
        $this->Newsletters = TableRegistry::get('Newsletters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Newsletters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Newsletters);
        $this->assertCrudDataIndex( 'index', $this->Newsletters);
      }
}
