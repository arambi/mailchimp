<?php
namespace Mailchimp\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Website\Lib\Website;
use DrewM\MailChimp\MailChimp;

/**
 * Mailchimp component
 */
class MailchimpComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
  protected $_defaultConfig = [];

  public $api;

  public function startup()
  {
    $this->api = new MailChimp( Website::get( 'settings.mailchimp'));
  }

  public function lists()
  {
    $lists = $this->api->get( 'lists');
    return $lists ['lists'];
  }

  public function createCampaing( array $options = [], $template_id = null)
  {
    $data = [
      'recipients' => [
        'list_id' => $list_id,
      ],
      'settings' => [
        'subject_line' => 'Mira qué bien',
        'title' => 'El título magnífico',
        'from_name' => 'Arambis',
        'reply_to' => 'arambi@gmail.com',
      ],
      'type' => 'regular',
    ];



    $templates = $this->api->get('templates', [
      'type' => 'user'
    ]);
  }

}
