<?php

namespace Mailchimp\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use I18n\Lib\Lang;
use Mailchimp\Traits\MailchimpTrait;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;

class NewslettersTable extends Table
{

  use MailchimpTrait;
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('mailchimp_newsletters');
    $this->setDisplayField('title');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

    // Behaviors
    $this->addBehavior('Manager.Crudable');
    $this->addBehavior('Block.Blockable', [
      'defaults' => []
    ]);

    $this->addBehavior('Cofree.Jsonable', [
      'fields' => ['settings', 'conditions']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud->addJsFiles([
      '/mailchimp/js/mc_directives.js',
    ]);

    $this->crud
      ->addFields([
        'title' => [
          'label' => __d('admin', 'Asunto del mail'),
          'template' => 'Mailchimp.fields/mc_subject',
        ],
        'locale' => [
          'type' => 'select',
          'options' => Lang::combine('iso2', 'name'),
          'label' => 'Idioma'
        ],
        'mc_preview' => [
          'type' => 'info',
          'template' => 'Mailchimp.fields/mc_preview',
          'show' => 'content.campaign'
        ],
        'schedule_time' => [
          'label' => __d('admin', 'Fecha y hora de envio'),
          'type' => 'datetime'
        ],
        'list_id' => [
          'type' => 'select',
          'options' => function ($crud) {
            $account = $this->mc()->get('/');
            $lists = $this->mc()->get('lists');
            return collection($lists['lists'])->combine('id', 'name');
          },
          'label' => __d('admin', 'Lista de correo'),
          'help' => __d('admin', 'Selecciona una lista de usuarios'),
          'change' => "cfSpinner.enable(); \$http.post( '/admin/mailchimp/newsletters/fields.json', {list_id: element.val()}).success(function( r){ cfSpinner.disable();  scope.data.merge_fields = r.merge_fields; scope.data.merge_fields_dropdown_options = r.merge_fields_dropdown_options; scope.data.segments = r.segments; scope.data.content.conditions = []})"
        ],
        'segment_id' => [
          'type' => 'info',
          'template' => 'Mailchimp.fields/mc_segment',
          'label' => 'Segmento',
        ],
        'conditions' => [
          'template' => 'Mailchimp.fields/mc_conditions',
          'label' => 'Condiciones',
          'type' => 'info',
          'show' => 'content.list_id && content.segment_id == "custom"'
        ],
        '_match' => [
          'type' => 'select',
          'label' => 'Condiciones a cumplir',
          'options' => [
            'any' => 'Cualquiera',
            'all' => 'Todas'
          ],
          'empty' => '-- Selecciona --',
          'show' => 'content.list_id && content.segment_id == "custom"'
        ]

      ])
      ->addIndex('index', [
        'fields' => $this->getIndexElements(),
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName([
        'singular' => __d('admin', 'Newsletters'),
        'plural' => __d('admin', 'Newsletters'),
      ])
      ->addView('create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => $this->getEditElements()
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])

      ->order([
        'schedule_time' => 'desc'
      ]);
  }

  private function getIndexElements()
  {
    $elements = [
      'title',
    ];

    if (!Configure::read('I18n.disable')) {
      $elements[] = 'locale';
    }

    return $elements;
  }

  private function getEditElements()
  {
    $elements = [
      'title',
      'mc_preview',
    ];

    if (!Configure::read('I18n.disable')) {
      $elements[] = 'locale';
    }

    $elements[] = 'schedule_time';
    $elements[] = 'list_id';

    if (!Configure::read('Newsletter.noConditions')) {
      $elements[] = 'segment_id';
      $elements[] = '_match';
      $elements[] = 'conditions';
    }

    $elements[] = 'blocks';

    return $elements;
  }

  public function afterDelete(Event $event, EntityInterface $entity)
  {
    if (!empty($entity->campaign_id)) {
      $this->mc()->delete('campaigns/' . $entity->campaign_id);
    }
  }

  public function findFront(Query $query)
  {
    return $query;
  }

  public function validationDefault(Validator $validator)
  {
    $validator
      ->notEmpty('list_id', __d('app', 'Es necesario indicar una lista'))
      ->notEmpty('locale', __d('app', 'Es necesario indicar un idioma'))
      ->requirePresence([
        'list_id' => [
          'mode' => 'create',
          'message' => 'Es necesario indicar una lista',
        ],
      ]);


    if (!Configure::read('I18n.disable')) {
      $validator->requirePresence([
        'locale' => [
          'mode' => 'create',
          'message' => 'Es necesario indicar un idioma',
        ],
      ]);
    }

    return $validator;
  }
}
