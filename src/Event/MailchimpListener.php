<?php 

namespace Mailchimp\Event;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Mailchimp\Traits\MailchimpTrait;
use Cake\Event\EventListenerInterface;
use Website\Lib\Website;

class MailchimpListener implements EventListenerInterface
{
  use MailchimpTrait;

  public function implementedEvents()
  {
    return [
      'Manager.View.Sites.beforeBuild' => 'websiteBuild',
      'Form.Controller.Forms.afterSend' => 'formAfterSend',
      'Store.Model.Order.finalizeOrder' => 'finalizeOrder',
    ];
  }

  public function websiteBuild( $event, $table)
  {
    $view = $event->subject();

    $table->crud->addFields([
      'settings.mailchimp' => [
        'type' => 'string',
        'label' => __d( 'admin', 'Introduce el key de Mailchimp'),
        'help' => __d( 'admin', "Introduce aquí el key de Mailchimp")
      ],
      'settings.mailchimp_reply_email' => [
        'type' => 'string',
        'label' => __d( 'admin', 'Correo electrónico de respuesta'),
        'help' => __d( 'admin', "Es la dirección de correo a la que podrán responder los usuarios que reciban los mails enviados con Mailchimp.")
      ],
      // 'settings.mailchimp_list' => [
      //   'type' => 'select',
      //   'options' => function( $crud) {
      //     try {
      //       $account = $this->mc()->get( '/');
      //       $lists = $this->mc()->get( 'lists');
      //       $options = array( '-- Ninguna --');
      //       $_options = collection( $lists ['lists'])->combine( 'id', 'name')->toArray();
      //       return array_merge( $options, $_options);
      //     } catch (\Throwable $th) {
      //       return [];
      //     }
      //   },
      //   'label' => __d( 'admin', 'Lista de Mailchimp para los usuarios'),
      //   'show' => 'content.settings.mailchimp'
      // ],
    ]);

    // $view->addFieldToView( ['create', 'update'], 'settings.mailchimp_list', 'settings.users_reply_email');

    $view->addColumn( 'update', [
      'title' => __d( 'admin', 'Mailchimp'),
      'box' => [
        [
          'elements' => [
            'settings.mailchimp',
            'settings.mailchimp_reply_email',
            // 'settings.mailchimp_list',
          ]
        ],
      ]
    ]);
  }

  public function formAfterSend( $event, $response)
  {
    if( !Website::get( 'settings.mailchimp'))
    {
      return;
    }

    if( !$response->form->has_mailchimp_list || empty( $response->form->mailchimp_list_id))
    {
      return;
    }
    
    $subscribe = collection( $response->response_fields)->firstMatch([
        'field.mailchimp_accept' => true
    ]);

    if( $subscribe && $subscribe->value != '1')
    {
      return;
    }

    $email = $this->findEmailInResponse( $response);
    
    if( empty( $email))
    {
      return;
    }

    $fields = $this->getFieldsInResponse( $response);

    try {
      $this->mc()->put( '/lists/'. $response->form->mailchimp_list_id .'/members/'. md5( $email), [
        'email_address' => $email,
        'status_if_new' => 'subscribed',
        'merge_fields' => $fields
      ]);
    } catch (\Throwable $th) {
      //throw $th;
    }
  }

  private function getFieldsInResponse( $response)
  {
    $return = [];

    foreach( $response->response_fields as $field)
    {
      $key = $field->field->mailchimp_field_id;

      if( !empty( $key) && $key != 'email')
      {
        $return [$key] = $field->value;
      }
    }

    return $return;
  }

  private function findEmailInResponse( $response)
  {
    foreach( $response->response_fields as $field)
    {
      if( $field->field->mailchimp_field_id == 'email')
      {
        return $field->value;
      }
    }
  }

  public function finalizeOrder( $event, $order)
  {
    if( !Website::get( 'settings.store_mailchimp') || empty( Website::get( 'settings.store_mailchimp_list')))
    {
      return;
    }

    if( !$order->subscribe_newsletter)
    {
      return;
    }

    $email = $order->adr_invoice_email;
    
    if( empty( $email))
    {
      return;
    }

    $order_fields = [
      'settings.store_mc_firstname' => 'adr_invoice_firstname',
      'settings.store_mc_lastname' => 'adr_invoice_lastname',
      'settings.store_mc_vat_number' => 'adr_invoice_vat_number',
      'settings.store_mc_address' => 'adr_invoice_address',
      'settings.store_mc_city' => 'adr_invoice_city',
      'settings.store_mc_postcode' => 'adr_invoice_postcode',
      'settings.store_mc_country' => function( $order){
        return $order->country_invoice->title;
      },
      'settings.store_mc_state' => function( $order){
        return $order->state_invoice->title;
      },
      'settings.store_mc_phone' => 'adr_invoice_phone',
    ];

    foreach( $order_fields as $key => $order_field)
    {
      if( !empty( Website::get( $key)))
      {
        if( is_callable( $order_field))
        {
          $value = $order_field( $order);
        }
        else
        {
          $value = $order->get( $order_field);
        }

        $fields [Website::get( $key)] = $value;
      }
    }

    try {
      $response = $this->mc()->put( '/lists/'. Website::get( 'settings.store_mailchimp_list') .'/members/'. md5( $email), [
        'email_address' => $email,
        'status_if_new' => 'subscribed',
        'merge_fields' => $fields
      ]);
    } catch (\Throwable $th) {
      //throw $th;
    } 
  }
} 