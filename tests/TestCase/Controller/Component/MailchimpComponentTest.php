<?php
namespace Mailchimp\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Mailchimp\Controller\Component\MailchimpComponent;

/**
 * Mailchimp\Controller\Component\MailchimpComponent Test Case
 */
class MailchimpComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Mailchimp\Controller\Component\MailchimpComponent
     */
    public $Mailchimp;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Mailchimp = new MailchimpComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mailchimp);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
