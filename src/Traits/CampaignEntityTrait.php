<?php 

namespace Mailchimp\Traits;

use Mailchimp\Traits\MailchimpTrait;


trait CampaignEntityTrait
{
  use MailchimpTrait;
  
  protected $_mcVirtual = [
    'campaign'
  ];

  private function setMcVirtuals()
  {
    $this->_virtual += $this->_mcVirtual;
  }

  protected function _getCampaign()
  {
    $campaign = $this->mc()->get( '/campaigns/'. $this->campaign_id);

    if( $campaign ['status'] == 404)
    {
      return null;
    }
  }

}