<?php
namespace Mailchimp\Model\Entity;

use Cake\ORM\Entity;

/**
 * MailchimpQueue Entity
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $method
 * @property string|null $data
 * @property bool $readed
 * @property \Cake\I18n\Time|null $readed_at
 * @property \Cake\I18n\Time|null $create
 * @property \Cake\I18n\Time|null $modified
 */
class MailchimpQueue extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'url' => true,
        'method' => true,
        'data' => true,
        'readed' => true,
        'readed_at' => true,
        'create' => true,
        'modified' => true,
    ];
}
