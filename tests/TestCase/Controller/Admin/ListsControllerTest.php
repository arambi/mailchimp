<?php
namespace Mailchimp\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Mailchimp\Controller\Admin\ListsController;

/**
 * Mailchimp\Controller\Admin\ListsController Test Case
 */
class ListsControllerTest extends IntegrationTestCase
{

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
