<?php
namespace Mailchimp\Controller;

use Mailchimp\Controller\AppController;

/**
 * Newsletters Controller
 *
 * @property \Mailchimp\Model\Table\NewslettersTable $Newsletters
 */
class NewslettersController extends AppController
{
  
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent('RequestHandler');

    if( isset( $this->Auth))
    {
      $this->Auth->allow();
    }
  }


  public function index( $id)
  {
    $this->viewBuilder()->layout( 'newsletter');
    $content = $this->Newsletters->find()
      ->where([
        'Newsletters.id' => $id
      ])
      ->first();

    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
