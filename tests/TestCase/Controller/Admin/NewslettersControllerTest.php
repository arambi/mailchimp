<?php
namespace Mailchimp\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Mailchimp\Controller\Admin\NewslettersController;

/**
 * Mailchimp\Controller\Admin\NewslettersController Test Case
 */
class NewslettersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.mailchimp.newsletters'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
