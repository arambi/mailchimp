<?php
namespace Mailchimp\Model\Entity;

use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Mailchimp\Traits\MailchimpTrait;
use Manager\Model\Entity\CrudEntityTrait;


class Newsletter extends Entity
{
  use CrudEntityTrait;
  use MailchimpTrait;

  protected $_accessible = [
    '*' => true,
    'rows' => true,
  ];

  protected $_virtual = [
    'conditions',
    'campaign',
    '_match'
  ];

  private $conditionTypes = [
    'birthday' => 'BirthdayMerge',
    'date' => 'DateMerge',
    'text' => 'TextMerge',
    'dropdown' => 'SelectMerge',
    'radio' => 'SelectMerge',
  ];

  protected function _getCampaign( $value)
  {
    if( $value !== null)
    {
      return $value;
    }

    if( empty( $this->campaign_id))
    {
      return;
    }
    
    $campaign = $this->mc()->get( '/campaigns/'. $this->campaign_id);

    $this->set( 'campaign', $campaign);
    return $campaign;
  }


  public function setCampaign()
  {
    if( !$this->campaign)
    {
      return;
    }

    if( !empty( $this->campaign ['recipients']['segment_opts']['conditions']))
    {
      $conditions = [];

      foreach( $this->campaign ['recipients']['segment_opts']['conditions'] as $condition)
      {
        $condition ['type'] = array_search( $condition ['condition_type'], $this->conditionTypes);
        unset( $condition ['condition_type']);
        $conditions [] = $condition;
      }

      $this->set( 'conditions', $conditions);
    }

    if( !empty( $this->campaign ['recipients']['segment_opts']['match']))
    {
      $this->set( '_match', $this->campaign ['recipients']['segment_opts']['match']);
    }    
  }

  public function mcOptions()
  {
    $return = [
      'settings' => [
        'subject_line' => $this->title,
        'title' => $this->title,
        'from_name' => $this->campaignFromName(),
        'reply_to' => Website::get( 'settings.mailchimp_reply_email') ? Website::get( 'settings.mailchimp_reply_email') : Website::get( 'email'),
      ],
      'recipients' => [
        'list_id' => $this->list_id,
      ],
    ];

    if( empty( $this->campaign_id))
    {
      $return ['type'] = 'regular';
    }
    
    if( !empty( $this->segment_id) || $this->segment_id == 0)
    {
      if( (int)$this->segment_id > 0)
      {
        $return ['recipients']['segment_opts'] = [
          'saved_segment_id' => (int)$this->segment_id
        ];
      }
      elseif( $this->segment_id == 'custom' && !empty( $this->conditions))
      {
        $return ['recipients']['segment_opts']['match'] = $this->_match;

        foreach( $this->conditions as $condition)
        {
          $condition = (array)$condition;
          $condition ['condition_type'] = $this->getConditionType( $condition ['type']);
          $return ['recipients']['segment_opts']['conditions'][] = $condition;
        }
      }
      elseif( empty( $this->segment_id))
      {
        $return ['recipients']['segment_opts'] = new \stdClass;
      }
    }

    return $return;
  }

  private function getConditionType( $type)
  {
    return $this->conditionTypes [$type];
  }

  public function campaignFromName()
  {
    $site = TableRegistry::get( 'sites_translations')->find()
      ->where([
        'id' =>  Website::get( 'id'),
        'locale' => Lang::getIso3( $this->locale)
      ])
      ->first();

    if( $site)
    {
      return $site->title;
    }

    return Website::get( 'title');
  }
}
