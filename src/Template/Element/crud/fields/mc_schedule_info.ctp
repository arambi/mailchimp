<div class="form-group" ng-if="content.id">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label"></label>
  <div class="col-lg-9">
    <div class="form-info"><a href ng-click="send( data.crudConfig.view.submit, data.content, content.long_archive_url)" target="_blank" class="btn btn-rounded btn-primary"><?= __d( 'admin', 'Previsualizar') ?></a></div>
  </div>
</div>
<div class="form-group" ng-if="content.id">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label"><?= __d( 'admin', 'Estado del envío') ?></label>
  <div class="col-lg-9">
    <div class="form-info" ng-if="content.status == 'save'"><span class="badge badge-success"><?= __d( 'admin', 'Editándose') ?></span></div>
    <div class="form-info" ng-if="content.status == 'paused'"><span class="badge badge-warning"><?= __d( 'admin', 'Pausado') ?></span></div>
    <div class="form-info" ng-if="content.status == 'schedule'"><span class="badge badge-info"><?= __d( 'admin', 'Listo para enviar') ?></span></div>
    <div class="form-info" ng-if="content.status == 'sending'"><span class="badge badge-danger"><?= __d( 'admin', 'Enviándose') ?></span></div>
    <div class="form-info" ng-if="content.status == 'sent'"><span class="badge badge-primary"><?= __d( 'admin', 'Enviado') ?></span></div>
  </div>
</div>

<div class="form-group"  ng-if="content.status == 'schedule' || content.status == 'sent'">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label"><?= __d( 'admin', 'Fecha de envío') ?></label>
  <div class="col-lg-9">
    <div class="form-info">{{ content.send_time | date : 'd/M/yyyy HH:mm' }}</div>
  </div>
</div>